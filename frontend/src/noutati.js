import React from 'react';
import BackgroundImg from './commons/images/poza.jpg';
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};
class Noutati extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }



    refresh(){
        this.forceUpdate()
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <h1 style={textStyle}>Noutăți</h1>
                <p  style={textStyle}> <b>
                    <ul>
                        <li>Am stabilit tehnologia cu care vom dezvolta aplicația, și anume TensorFlow</li>
                        <li>Am instalat Anaconda pe Windows</li>
                        <li>Am început documentarea legată de concepte de ML, Python și TensorFlow</li>
                    </ul>
                </b></p>
                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Noutati;
