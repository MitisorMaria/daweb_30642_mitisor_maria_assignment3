<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AppUserController extends Controller
{
    public function index()
    {
        $app_users = AppUser::index();
        return response()->json($app_users);
    }



    public function store(Request $request)
    {
        $request->validate([
            'username'        =>      'required',
            'email'             =>      'required',
            'password'          =>      'required',
            'image'             =>      'required'
        ]);

        $input = $request->all();
        $img_stripped = str_replace("data:image/png;base64,", "", $request->image);

        $img_decoded = base64_decode($img_stripped);
        echo ($img_stripped);
        // if validation success then create an input array
        $inputArray      =           array(
            'username'        =>      $request->username,
            'email'             =>      $request->email,
            'image'         => $img_decoded,
            'password'          =>      Hash::make($request->password)
        );

        // register user
        $user           =           User::create($inputArray);

        // if registration success then return with success message
        if(!is_null($user)) {
            return back()->with('success', 'You have registered successfully.');
        }

        // else return with error message
        else {
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }
    }


    public function show(Request $request)
    {
            $users = User::all();
            $name = $request->username;
            foreach ($users as $user):
                {
                    if ($user->username == $name){
                        $user_json = clone $user;
                        $image_encoded = base64_encode($user->image);
                        $user_json->image = $image_encoded;
                        return $user_json;
                    }
                }
            endforeach;
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
    }


    public function validateUser(Request $request)
    {
        $request->validate([
            "username"           =>    "required",
            "password"        =>    "required"
        ]);

        $userCredentials = $request->only('username', 'password');


        // check user using auth function
        if (Auth::attempt($userCredentials)) {
            Session::put('username',$request->username);
            Session::put('login',TRUE);
            Session::start();
            return response()->json([
                'app_user' => $userCredentials]);
        }
    }


    public function update(Request $request, User $app_user)
    {
        $request->validate([
            'email' => 'required' //optional if you want this to be required
        ]);

        $img_stripped = str_replace("data:image/png;base64,", "", $request->image);

        $img_decoded = base64_decode($img_stripped);

        $users = User::all();
        $theUser = $users->first;
        $name = $request->username;
        $password = $request->password;
        foreach ($users as $user):
            {
                if ($user->username == $name){
                    $theUser = $user;
                    break;
                }
            }
        endforeach;

        $theUser->email = $request->email;
        $theUser->password = Hash::make($password);
        $theUser->image = $img_decoded;
        $theUser->update();
        return response()->json([
            'message' => 'app_user updated!'
        ]);
    }

    public function logout(){
        Session::flush();
        Auth::logout();
    }

    public function destroy(AppUser $app_user)
    {
        $app_user->delete();
        return response()->json([
            'message' => 'app_user deleted'
        ]);

    }
}
