import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Noutati from './noutati'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Despre from "./despre";
import Coordonator from "./coordonator";
import Profil from "./pagProfil";
import ProfilStudent from "./profil";
import Contact from "./contact";
import Acasa from "./acasa";
import Login from "./login";
import Register from "./register";
import Edit from "./edit";


let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Login/>}
                        />

                        <Route
                            exact
                            path='/register'
                            render={() => <Register/>}
                        />

                        <Route
                            exact
                            path='/edit'
                            render={() => <Edit/>}
                        />

                        <Route
                            exact
                            path='/pagProfil'
                            render={() => <Profil/>}
                        />

                        <Route
                            exact
                            path='/acasa'
                            render={() => <Acasa/>}
                        />

                        <Route
                            exact
                            path='/despre'
                            render={() => <Despre/>}
                        />

                        <Route
                            exact
                            path='/profilStudent'
                            render={() => <ProfilStudent/>}
                        />

                        <Route
                            exact
                            path='/coordonator'
                            render={() => <Coordonator/>}
                        />

                        <Route
                            exact
                            path='/contact'
                            render={() => <Contact/>}
                        />

                        <Route
                            exact
                            path='/noutati'
                            render={() => <Noutati/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
