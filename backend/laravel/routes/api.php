<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AppUserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/app_users', 'AppUserController@all')->name('AppUserController.index');

Route::post('/app_users', 'AppUserController@store')->name('AppUserController.store');

Route::middleware('\Fruitcake\Cors\HandleCors::class')->get('/app_users', 'AppUserController@show')->name('AppUserController.show');

Route::middleware('\Fruitcake\Cors\HandleCors::class')->post('/app_users/validate', 'AppUserController@validateUser');

Route::put('/app_users/', 'AppUserController@update')->name('AppUserController.update');

Route::post('/app_users/logout', 'AppUserController@logout')->name('AppUserController.logout');

Route::get('/comments', 'CommentController@index')->name('comments.index');

Route::post('/comments', 'CommentController@store')->name('comments.store');
