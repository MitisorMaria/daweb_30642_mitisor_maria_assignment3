import React from 'react';
import counterpart from 'counterpart';
import BackgroundImg from './commons/images/poza.jpg';
import Translate from 'react-translate-component';
import eng from './lang/eng';
import ro from './lang/ro';
import axios from 'axios';
import {HOST} from './commons/hosts';


import './person-data/person/fields/fields.css';
import Button from "react-bootstrap/Button";


const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`,
    display: 'flex',
    flexDirection: 'column'
};

const textStyle = {color: 'white', align: 'middle'};

counterpart.registerTranslations('eng', eng);
counterpart.registerTranslations('ro', ro);
counterpart.setLocale(localStorage.getItem("lang"));



class Profil extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
            lang: "ro",
            username: "",
            email: "",
            image: ''
        };

        this.getUserInfo = this.getUserInfo.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    componentDidMount() {
        const lang = localStorage.getItem("lang");
        if(lang) {
            this.setState({ lang: lang});
            counterpart.setLocale(lang);
        }
        this.getUserInfo();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const lang = localStorage.getItem("lang");
        counterpart.setLocale(lang);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }



    getUserInfo(){
        const xmlhttp = new XMLHttpRequest();
        const username = sessionStorage.getItem('username');
        let email="";
        let image="";
        const theUrl = HOST.laravel_api+"?username="+username;
        xmlhttp.open("GET", theUrl, false);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xmlhttp.setRequestHeader("Cookie", "login=TRUE");

        xmlhttp.onload = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200 && xmlhttp.response) {
                    const obj= JSON.parse(xmlhttp.response);
                    email = obj.email;
                    image = obj.image;
            } else {
                window.alert("Error " + xmlhttp.status + " din pag profil");
            }
        }

        xmlhttp.send();
        this.setState({username: username, email: email, image: image});
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <h1 style={textStyle}>Profilul meu</h1>
                <p>
                    <Button href={"/edit"}>Editează informațiile</Button>
                </p>
                <p style={textStyle}>Username: {this.state.username}</p>
                <p style={textStyle}>Email: {this.state.email}</p>
                <p style={textStyle}>Imagine de profil:</p>
                <img src={`data:image/jpeg;base64,${this.state.image}`} width={"25%"}/>



                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Profil;
