import {HOST} from './commons/hosts';
import RestApiClient from "./commons/api/rest-client";


const endpoint = {
    post_mail: "/"
};


function postMail(subject, body, callback){
    let request = new Request(HOST.backend_api + endpoint.post_mail , {
        method: 'POST',
        params : {
            subject: subject.toString(),
            body: body.toString()
        }
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    postMail
};
