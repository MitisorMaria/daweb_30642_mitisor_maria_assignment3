import React from 'react';
import Button from "react-bootstrap/Button";
import BackgroundImg from "./commons/images/poza.jpg";
import TextInput from "./person-data/person/fields/TextInput";
import validate from "./person-data/person/validators/person-validators";
import {HOST} from "./commons/hosts";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`,
    display: 'flex',
    flexDirection: 'column'
};

const textStyle = {color: 'white', align: 'middle'};

class Register extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            image:'',
            errorStatus: 0,
            error: null,
            username: {
                value: '',
                placeholder: 'Enter username',
            },

            email: {
                value: '',
                placeholder: 'Email...',
            },

            password: {
                value: '',
                placeholder: 'Password...',
            },

        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.registered = this.registered.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    createImage(file) {
        let reader = new FileReader();
        reader.onload = (e) => {
            this.setState({
                image: e.target.result
            })
        };
        reader.readAsDataURL(file);
    }

    handleUpload(event) {
        let files = event.target.files || event.dataTransfer.files;
        if (!files.length)
            return;
        const image = this.createImage(files[0]);
    }


    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }


    registerUser(){
        sessionStorage.removeItem('register');
        const username = this.state.username;
        const password = this.state.password;
        const email = this.state.email;
        const image = this.state.image;
        if (username && password && email) {
            sessionStorage.setItem("username", username);
            const xmlhttp = new XMLHttpRequest();
            const theUrl = HOST.laravel_api;
            xmlhttp.open("POST", theUrl, false);
            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            xmlhttp.onload = function () {
                if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
                    window.alert("Done!");
                } else {
                    window.alert("Error " + xmlhttp.status);
                }
            }
            xmlhttp.send(JSON.stringify({"username": username, "password": password, "email": email, "image": image}));
        }
    }


    registered(){
        sessionStorage.removeItem('register');
    }

    handleSubmit(){

    }


    render() {
        return (
            <div style={backgroundStyle}>
                <h1 style={textStyle}>Register</h1>
                <form>
                    <input name="username"
                               placeholder={this.state.username.placeholder}
                               value={this.state.username.value}
                               onChange={this.handleChange}
                    />


                    <input name="password" type="password"
                               placeholder={this.state.password.placeholder}
                               value={this.state.password.value}
                               onChange={this.handleChange}
                    />


                    <input name="email"
                               placeholder={this.state.email.placeholder}
                               value={this.state.email.value}
                               onChange={this.handleChange}
                    />

                    <input type="file" onChange={this.handleUpload} />
                    <Button name="register" href={"/pagProfil"} onClick={this.registerUser}>Register</Button>
                    <Button name="back" href={"/"} onClick={this.registered}>Back</Button>
                </form>

            </div>
        );
    }
}

export default Register;
